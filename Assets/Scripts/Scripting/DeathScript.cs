﻿using System;
using Spineless;
using UnityEngine;


/// <summary>
/// Enumeration of actions that can occur on Actor's death.
/// </summary>
[Serializable]
public class DeathScript
{

	#region Fields
	[SerializeField] private ScriptedAction effect;
	[SerializeField] private string additionalParameters;
	#endregion

	#region Properties
	/// <summary>
	/// Action to take when ItemScript is trigger.
	/// </summary>
	public ScriptedAction Effect
	{
		get { return this.effect; }
	}


	/// <summary>
	/// Parameters to pass to ScriptedAction.
	/// </summary>
	public string AdditionalParameters
	{
		get { return this.additionalParameters; }
	}
	#endregion


	/// <summary>
	/// Activate any scripts on this item possessing the specified trigger.
	/// </summary>
	/// <param name="trigger">Possible trigger.</param>
	/// <param name="parameters">What triggered the script.</param>
	/// <returns>True if any scripts were actived.</returns>
	public void Execute(object parameters)
	{
		this.Effect.Execute(parameters, this.additionalParameters);
	}
}