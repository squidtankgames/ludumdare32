﻿using System;
using Spineless;
using UnityEngine;


/// <summary>
/// Behavior logic that will be possessed by items.
/// </summary>
[Serializable]
public class ItemScript
{

	#region Fields
	[SerializeField] private Trigger scriptTrigger;
	[SerializeField] private ScriptedAction effect;
	[SerializeField] private String parameters;
	#endregion


	#region Enums
	/// <summary>
	/// Possible triggers for an ItemScript.
	/// </summary>
	public enum Trigger
	{
		OnAttackInitiated,
		OnAttackTerminated,
		OnCollected,
		OnDestroyed,
		OnDropped,
		OnEquipped,
		OnInstantiated,
		OnLevelEnded,
		OnLevelStarted,
		OnUnequipped,
		OnUsed
	}
	#endregion


	#region Properties
	/// <summary>
	/// What will trigger the ItemScript's effect.
	/// </summary>
	public Trigger ScriptTrigger
	{
		get { return this.scriptTrigger; }
	}


	/// <summary>
	/// Action to take when ItemScript is trigger.
	/// </summary>
	public ScriptedAction Effect
	{
		get { return this.effect; }
	}


	/// <summary>
	/// Parameters to pass to ScriptedAction.
	/// </summary>
	public string Parameters
	{
		get { return this.parameters; }
	}
	#endregion

}


public static class ItemScriptEffects
{

	// TODO: Need some way to override this. May need a new enum of possible effects that can map to ScriptedActions.
	//public static void Execute(this ScriptedAction effect)
		
}