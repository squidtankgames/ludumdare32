﻿using Spineless.Scripting;


public class SetFlag : GameScript
{

	public override void Execute(object activator, string[] parameters)
	{
		BacterioRage.Game.GameFlags.Add(parameters[0]);
	}

}
