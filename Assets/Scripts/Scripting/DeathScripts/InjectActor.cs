﻿using System.Runtime.CompilerServices;
using Spineless;
using Spineless.Platformer;
using Spineless.Scripting;
using UnityEngine;


public class InjectActor : GameScript
{

	private bool isZooming;
	private ActorComponent killedActor;
	private GameObject killer;
	private float originalCameraSize;
	private float zoomedCameraSize;
	private float smoothing = 10;
	private Camera mainCamera;

	public void Update()
	{
		if (this.isZooming)
		{
			this.mainCamera.orthographicSize = Mathf.Lerp(
				this.mainCamera.orthographicSize,
				this.zoomedCameraSize,
				Time.deltaTime * this.smoothing);
		}
			
	}

	public override void Execute(object activator, string[] parameters)
	{
		
		this.killedActor = ((GameObject) activator).GetComponent<ActorComponent>();
		this.killer = killedActor.LastAttacker;
		killedActor.LastAttacker = null;

		// Wasn't killed by injection.
		if (this.killer == null)
		{
			this.gameObject.Destroy();
			return;
		}
			

		GameObject target = GameObject.FindGameObjectWithTag(parameters[0]);

		this.mainCamera = Camera.main;
		this.originalCameraSize = mainCamera.orthographicSize;
		if (parameters.Length > 1)
			this.zoomedCameraSize = float.Parse(parameters[1]);
		else
			this.zoomedCameraSize = this.originalCameraSize;

		// Pause enemy, and zoom camera.
		this.isZooming = true;
		this.killedActor.GetComponent<AI>().enabled = false;
		this.killedActor.GetComponent<PhysicsBody>().enabled = false;
		
		// Return to normal after zoom.
		Coroutines.Wait(500, () =>
		{
			killer.transform.position = target.transform.position;
			mainCamera.transform.position = new Vector3(
			target.transform.position.x,
			target.transform.position.y,
			mainCamera.transform.position.z);
			this.isZooming = false;
			mainCamera.orthographicSize = this.originalCameraSize;
			this.killedActor.GetComponent<AI>().enabled = true;
			this.killedActor.GetComponent<PhysicsBody>().enabled = true;
			this.gameObject.Destroy();
		});
		
	}

	
}