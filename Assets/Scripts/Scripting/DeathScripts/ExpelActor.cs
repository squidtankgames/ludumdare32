﻿using Spineless;
using Spineless.Scripting;
using UnityEngine;


public class ExpelActor : GameScript
{

	private ActorComponent killedActor;
	private GameObject killer;


	public override void Execute(object activator, string[] parameters)
	{
		this.killedActor = ((GameObject) activator).GetComponent<ActorComponent>();
		this.killer = killedActor.LastAttacker;

		// Wasn't killed by injection.
		if (this.killer == null)
		{
			this.killedActor.Health.Current = 1;
			this.gameObject.Destroy();
			return;
		}

		// Heal player.
		this.killer.GetComponent<ActorComponent>().Health.Current =
			this.killer.GetComponent<ActorComponent>().Health.Maximum;

		Coroutines.Wait(1000, () =>
		{
			GameObject target = GameObject.FindGameObjectWithTag(parameters[0]);
			//this.killer.transform.position = target.transform.position;
			target.Destroy();
			this.killedActor.GameObject.Destroy();
			BacterioRage.Notification.Transform.GetChild(0).gameObject.SetActive(true);
			BacterioRage.Notification.ShowMessage("You Gain:\n" + parameters[1], null);
		});
		this.gameObject.Destroy();
	}

}