﻿namespace Spineless.Platformer
{
	/// <summary>
	/// String constants for typical commands.
	/// </summary>
	public static class Commands
	{
		public const string Dash = "dash";
		public const string Jump = "jump";
		public const string MoveRight = "move right";
		public const string MoveLeft = "move left";
		public const string Run = "run";
		public const string Attack = "attack";
		public const string Lift = "lift";
		public const string Inject = "inject";
	}
}