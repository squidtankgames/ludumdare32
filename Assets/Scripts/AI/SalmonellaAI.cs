﻿using System;

using Spineless;
using Spineless.Platformer;
using UnityEngine;


public class SalmonellaAI : AI
{

	#region Fields
	private System.Random random = new System.Random();
	private PhysicsBody physicsBody;
	#endregion


	#region Enums
	private enum SalmonellaAction
	{
		Idle,
		WalkLeft,
		WalkRight,
		ShootMissile
	}
	#endregion


	public override void FixedUpdate()
	{
		
	}


	public override void ProcessAI()
	{
		DetermineAction(inputReceiver);
	}


	private SalmonellaAction RandomAction()
	{
		Array values = Enum.GetValues(typeof (SalmonellaAction));
		return (SalmonellaAction) values.GetValue(this.random.Next(values.Length));
	}


	private void DetermineAction(InputReceiver inputReceiver)
	{
		float random = this.random.Next(100);
		if (random > 2)
			return;

		// Currently idle, pick a new command.
		if (this.currentCommand == null)
		{
			switch (RandomAction())
			{
				case SalmonellaAction.WalkLeft:
					this.currentCommand = new AICommand(Commands.MoveLeft, CommandState.Begin);
					break;
				case SalmonellaAction.WalkRight:
					this.currentCommand = new AICommand(Commands.MoveRight, CommandState.Begin);
					break;
				case SalmonellaAction.Idle:
				case SalmonellaAction.ShootMissile:
					this.currentCommand = new AICommand(Commands.Attack, CommandState.Begin);
					break;
			}
			if (this.currentCommand != null)
				this.inputReceiver.InputReceived(this.currentCommand.Command, this.currentCommand.State);
		}
		else
		{
			this.inputReceiver.InputReceived(this.currentCommand.Command, CommandState.End);
			this.currentCommand = null;
		}
	}

}

