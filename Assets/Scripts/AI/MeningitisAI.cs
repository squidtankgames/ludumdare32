﻿using System;

using Spineless;
using Spineless.Platformer;
using UnityEngine;


public class MeningitisAI : AI
{

	#region Fields
	private System.Random random = new System.Random();
	private PhysicsBody physicsBody;
	#endregion


	#region Enums
	private enum MeningitisAction
	{
		Idle,
		WalkLeft,
		WalkRight,
		ShootMissile
	}
	#endregion


	public void Start()
	{
		base.Start();
		this.physicsBody = this.GetComponent<PhysicsBody>();
	}

	public override void FixedUpdate()
	{
		float fly = random.Next(250);
		float flyDirection = random.Next(100);
		if (flyDirection < 50)
		{
			this.physicsBody.Velocity += new Vector2(0f, fly);	
		}
		else
		{
			this.physicsBody.Velocity -= new Vector2(0f, fly);
		}

	}


	public override void ProcessAI()
	{
		DetermineAction(inputReceiver);
	}


	private MeningitisAction RandomAction()
	{
		Array values = Enum.GetValues(typeof (MeningitisAction));
		return (MeningitisAction) values.GetValue(this.random.Next(values.Length));
	}


	private void DetermineAction(InputReceiver inputReceiver)
	{
		float random = this.random.Next(100);
		if (random > 2)
			return;

		// Currently idle, pick a new command.
		if (this.currentCommand == null)
		{
			switch (RandomAction())
			{
				case MeningitisAction.WalkLeft:
					this.currentCommand = new AICommand(Commands.MoveLeft, CommandState.Begin);
					break;
				case MeningitisAction.WalkRight:
					this.currentCommand = new AICommand(Commands.MoveRight, CommandState.Begin);
					break;
				case MeningitisAction.Idle:
				case MeningitisAction.ShootMissile:
					this.currentCommand = new AICommand(Commands.Attack, CommandState.Begin);
					break;
			}
			if (this.currentCommand != null)
				this.inputReceiver.InputReceived(this.currentCommand.Command, this.currentCommand.State);
		}
		else
		{
			this.inputReceiver.InputReceived(this.currentCommand.Command, CommandState.End);
			this.currentCommand = null;
		}
	}

}

