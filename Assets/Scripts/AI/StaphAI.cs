﻿using System;
using Spineless;
using Spineless.Platformer;
using UnityEngine;


public class StaphAI : AI
{

	#region Fields
	private System.Random random = new System.Random();
	private PhysicsBody physicsBody;
	#endregion


	#region Enums
	private enum StaphAction
	{
		Idle,
		WalkLeft,
		WalkRight
	}
	#endregion


	public override void FixedUpdate()
	{
		AvoidCliffs(inputReceiver);
	}


	public override void ProcessAI()
	{
		DetermineAction(inputReceiver);
	}


	private StaphAction RandomAction()
	{
		Array values = Enum.GetValues(typeof (StaphAction));
		return (StaphAction) values.GetValue(this.random.Next(values.Length));
	}


	private void DetermineAction(InputReceiver inputReceiver)
	{
		float random = this.random.Next(100);
		if (random > 5)
			return;

		// Currently idle, pick a new command.
		if (this.currentCommand == null)
		{
			switch (RandomAction())
			{
				case StaphAction.Idle:
					break;
				case StaphAction.WalkLeft:
					this.currentCommand = new AICommand(Commands.MoveLeft, CommandState.Begin);
					break;
				case StaphAction.WalkRight:
					this.currentCommand = new AICommand(Commands.MoveRight, CommandState.Begin);
					break;
			}
			if (this.currentCommand != null)
				inputReceiver.InputReceived(this.currentCommand.Command, this.currentCommand.State);
		}
		else
		{
			inputReceiver.InputReceived(this.currentCommand.Command, CommandState.End);
			this.currentCommand = null;
		}
	}


	private void AvoidCliffs(InputReceiver inputReceiver)
	{
		if (this.currentCommand == null)
			return;

		PhysicsBody physicsBody = inputReceiver.GetComponent<PhysicsBody>();

		// Don't walk off cliffs.
		switch (physicsBody.RaycastCliffCheck())
		{
			case Direction.Left:
				//Debug.Log("Detected cliff edge on left.");
				if (this.currentCommand.Command.Equals(Commands.MoveLeft)
					&& this.currentCommand.State == CommandState.Begin)
				{
					inputReceiver.InputReceived(this.currentCommand.Command, CommandState.End);
					this.currentCommand = null;
				}
				break;

			case Direction.Right:
				//Debug.Log("Detected cliff edge on right.");
				if (this.currentCommand.Command.Equals(Commands.MoveRight)
					&& this.currentCommand.State == CommandState.Begin)
				{
					//inputReceiver.GetComponent<BoxCollider2D>().isTrigger = false;
					inputReceiver.InputReceived(this.currentCommand.Command, CommandState.End);
					this.currentCommand = null;
				}
				break;
		}
	}

}

