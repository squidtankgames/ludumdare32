﻿using Spineless;
using UnityEngine;


public abstract class AI : GameComponent
{

	#region Fields
	[SerializeField] protected InputReceiver inputReceiver;
	[SerializeField] protected float constantDelay;
	[SerializeField] protected float deviation;

	protected AICommand currentCommand;
	#endregion


	public void Start()
	{
		this.inputReceiver = this.GetComponent<InputReceiver>();
	}


	#region Updates
	/// <summary>
	/// Capture input, notify all registered IInputReceiver objects.
	/// </summary>
	public virtual void Update()
	{
		ProcessAI();
	}


	/// <summary>
	/// Not used.
	/// </summary>
	public virtual void FixedUpdate() { ;}
	#endregion


	public abstract void ProcessAI();


	public class AICommand
	{
		public string Command;
		public CommandState State;

		public AICommand(string command, CommandState state)
		{
			this.Command = command;
			this.State = state;
		}
	}

}

