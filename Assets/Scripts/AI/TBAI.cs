﻿using System;

using Spineless;
using Spineless.Platformer;
using UnityEngine;


public class TBAI : AI
{

	#region Fields
	private System.Random random = new System.Random();
	private PhysicsBody physicsBody;
	#endregion


	#region Enums
	private enum TBAction
	{
		Idle,
		WalkLeft,
		WalkRight
	}
	#endregion


	public void Start()
	{
		base.Start();
		this.physicsBody = this.GetComponent<PhysicsBody>();
	}

	public override void FixedUpdate()
	{
		if (this.physicsBody.Grounded)
		{
			if (random.Next(100) < 75)
				return;

			float bounce = random.Next(1500);
			this.physicsBody.Velocity += new Vector2(0f, bounce);	
		}
	}


	public override void ProcessAI()
	{
		DetermineAction(inputReceiver);
	}


	private TBAction RandomAction()
	{
		Array values = Enum.GetValues(typeof (TBAction));
		return (TBAction) values.GetValue(this.random.Next(values.Length));
	}


	private void DetermineAction(InputReceiver inputReceiver)
	{
		float random = this.random.Next(100);
		if (random > 2)
			return;

		// Currently idle, pick a new command.
		if (this.currentCommand == null)
		{
			switch (RandomAction())
			{
				case TBAction.WalkLeft:
					this.currentCommand = new AICommand(Commands.MoveLeft, CommandState.Begin);
					break;
				case TBAction.WalkRight:
					this.currentCommand = new AICommand(Commands.MoveRight, CommandState.Begin);
					break;
				case TBAction.Idle:
					break;
			}
			if (this.currentCommand != null)
				this.inputReceiver.InputReceived(this.currentCommand.Command, this.currentCommand.State);
		}
		else
		{
			this.inputReceiver.InputReceived(this.currentCommand.Command, CommandState.End);
			this.currentCommand = null;
		}
	}

}

