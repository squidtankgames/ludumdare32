﻿
using System.Runtime.CompilerServices;
using Spineless;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHUD : GameComponent
{
	private GameObject player;
	public Text healthIndicator;


	public GameObject Player
	{
		get { return player; }
		set
		{
			player = value;
		}
	}

	public void Update()
	{
		if (this.player != null)
		{
			if (this.healthIndicator != null)
				this.healthIndicator.text = player.GetComponent<ActorComponent>()
					.Health.Current.ToString();
		}
	}

}
