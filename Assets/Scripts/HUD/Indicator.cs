﻿using Spineless;
using UnityEngine;


public class Indicator : GameComponent
{

	public GameObject indicator;

	public void OnTriggerEnter2D(Collider2D collider)
	{
		if (collider.gameObject.name.ToLower().Contains("player"))
			this.indicator.SetActive(true);
	}

	public void OnTriggerExit2D(Collider2D collider)
	{
		if (collider.gameObject.name.ToLower().Contains("player"))
			this.indicator.SetActive(false);
	}

}
