﻿using System;

using Spineless;

using UnityEngine;


/// <summary>
/// Represents an item in the game world and the quantity of its stack.
/// </summary>
[Serializable]
public class ItemRecord :
	ICloneable
{
		
	#region Fields
	[SerializeField] private int itemId;
	[SerializeField] private int quantity;
	[SerializeField] private bool droppable;
	private ItemComponent sceneInstance;

	private ItemData data;
	#endregion


	#region Properties
	/// <summary>
	/// Get the ItemComponent associated with this record's ID.
	/// </summary>
	public ItemData Data
	{
		get
		{
			if (this.data == null)
				this.data = BacterioRage.Game.StaticData.Items.GetByID(this.itemId);
			return this.data;
		}
	}

	/// <summary>
	/// Whether the item can be dropped, either by choice or upon death.
	/// </summary>
	public bool Droppable
	{
		get { return this.droppable; }
		set { this.droppable = value; }
	}


	/// <summary>
	/// Unique ID of the ItemComponent this record represents.
	/// </summary>
	public int ItemId
	{
		get { return this.itemId; }
	}


	/// <summary>
	/// Quantity of the item in the current stack.
	/// </summary>
	public int Quantity
	{
		get { return this.quantity; }
	}


	/// <summary>
	/// record of the item in the scene.
	/// </summary>
	public ItemComponent SceneInstance
	{
		get { return this.sceneInstance; }
		set { this.sceneInstance = value; }
	}
	#endregion	

	
	#region ICloneable
	public object Clone()
	{
		return this.MemberwiseClone();
	}
	#endregion


	/// <summary>
	/// Reduce the quantity of the item stack.
	/// </summary>
	/// <param name="value">Amount to decrease the item quantity.</param>
	/// <returns>Amount the quantity was successfully decreased.</returns>
	public int DecrementQuantity(int value)
	{
		// Don't decrement negative numbers.
		if (value < 0)
		{
			Debug.LogWarning("ItemRecord.DecrementQuantity: Tried to decrement negative amount.");
			return 0;
		}

		int previousQuantity = this.quantity;
		this.quantity -= value;

		// Can't decrement below 0.
		if (this.quantity < 0)
			this.quantity = 0;

		// Return how many items were removed, if any.
		return previousQuantity - this.quantity;
	}


	/// <summary>
	/// Increase the quantity of the item stack.
	/// </summary>
	/// <param name="value">Amount to increase the item.</param>
	/// <returns>Amount the stack was successfully increased.</returns>
	public int IncrementQuantity(int value)
	{
		// Don't increment negative numbers.
		if (value < 0)
		{
			Debug.LogWarning("ItemRecord.IncrementQuantity: Tried to increment negative amount. Use DecrementQuantity, instead.");
			return 0;
		}

		int previousQuantity = this.quantity;
		this.quantity += value;

		// Hit capacity, set to max quantity instead.
		if (this.quantity > this.Data.MaxQuantity)
			this.quantity = this.Data.MaxQuantity;

		// Return how many items were added, if any.
		return this.quantity - previousQuantity;
	}

}