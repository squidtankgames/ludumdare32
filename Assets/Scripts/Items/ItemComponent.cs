﻿using Spineless;
using UnityEngine;


public class ItemComponent : GameComponent
{

	#region Fields
	[SerializeField] private ItemRecord record;
	#endregion

		
	#region Properties
	public ItemRecord Record
	{
		get { return this.record; }
	}
	#endregion


	#region Initialization
	public virtual void Awake()
	{
		this.record.SceneInstance = this;
	}
	#endregion

}
