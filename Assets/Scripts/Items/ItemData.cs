﻿using System;

using Spineless;
using UnityEngine;


[Serializable]
public class ItemData
{

	#region Fields
	[SerializeField] private int id;
	[SerializeField] private string name;
	[SerializeField] private ItemType type;
	[SerializeField] private string description;
	[SerializeField] private int soulValue;
	[SerializeField] private int maxQuantity;
	[SerializeField] private StringDictionary properties;
	[SerializeField] private Texture2D inventoryIcon;
	[SerializeField] private GameObject prefab;
	[SerializeField] private ItemScript[] scripts;
	#endregion


	#region Properties
	/// <summary>
	/// Get item properties as typed ArmorProperties.
	/// </summary>
	public ArmorProperties ArmorProperties
	{
		get { return new ArmorProperties(this.properties); }
	}

	/// <summary>
	/// Description of the item.
	/// </summary>
	public string  Description
	{
		get { return this.description; }
	}
		

	/// <summary>
	/// Unique ID for item.
	/// </summary>
	public int Id
	{
		get { return this.id; }
	}


	/// <summary>
	/// Icon representing this item in inventory.
	/// </summary>
	public Texture2D InventoryIcon
	{
		get { return this.inventoryIcon; }
	}


	/// <summary>
	/// Maximum quantity of the item in a "stack."
	/// </summary>
	public int MaxQuantity
	{
		get { return this.maxQuantity; }
	}
		

	/// <summary>
	/// Name of the item.
	/// </summary>
	public string Name
	{
		get { return this.name; }
	}


	/// <summary>
	/// Linked prefab for the item, for instantiating into scenes.
	/// </summary>
	public GameObject Prefab
	{
		get { return this.prefab; }
	}


	/// <summary>
	/// Additional data for the item based on type.
	/// Use GetProperties to retrieve typed object properties.
	/// </summary>
	public StringDictionary Properties
	{
		get { return properties; }
	}


	/// <summary>
	/// Additional logic to be taken on certain events for the item.
	/// </summary>
	public ItemScript[] Scripts
	{
		get { return this.scripts; }
	}


	/// <summary>
	/// Category the item falls into.
	/// ie: Armor, MeleeWeaponComponent, Story, etc.
	/// </summary>
	public ItemType Type
	{
		get { return this.type; }
	}


	/// <summary>
	/// Absolute market value of the item.
	/// Any other values (buy/sell) should derive from this value.
	/// </summary>
	public int SoulValue
	{
		get { return this.soulValue; }
	}


	/// <summary>
	/// Get item properties as typed WeaponProperties.
	/// </summary>
	public WeaponProperties WeaponProperties
	{
		get { return new WeaponProperties(this.properties); }
	}
	#endregion


	#region Instantiate()
	/// <summary>
	/// Instantiate item's prefab into the scene.
	/// </summary>
	/// <returns>Instantiated GameObject.</returns>
	public GameObject Instantiate()
	{
		GameObject instance = this.prefab.Instantiate();
		for (int i = 0; i < this.scripts.Length; i++)
		{
			// TODO: Activate ItemScript
			// this.scripts[i].OnInstantiated(instance);
		}

		return instance;
	}


	/// <summary>
	/// Instantiate item's prefab into the scene at the location/rotation of a transform.
	/// </summary>
	/// <param name="transform">Transform that item will be instantiated at.</param>
	/// <returns></returns>
	public GameObject Instantiate(Transform transform)
	{
		return Instantiate(transform.position, transform.rotation);
	}


	/// <summary>
	/// Instantiate item's prefab into the scene at a specific position and rotation.
	/// </summary>
	/// <param name="position">Vector2 location to instantiate item to.</param>
	/// <param name="rotation">Rotation of item once instantiated.</param>
	/// <returns>Instantiated GameObject.</returns>
	public GameObject Instantiate(Vector2 position, Quaternion rotation)
	{
		GameObject instance = this.prefab.Instantiate(position, rotation);
		for (int i = 0; i < this.scripts.Length; i++)
		{
			// TODO: Activate ItemScript
			// this.scripts[i].OnInstantiated(instance);
		}

		return instance;
	}
	#endregion


	/// <summary>
	/// Activate any scripts on this item possessing the specified trigger.
	/// </summary>
	/// <param name="trigger">Possible trigger.</param>
	/// <param name="activator">What triggered the script.</param>
	/// <returns></returns>
	public bool ActivateScript(ItemScript.Trigger trigger, object activator)
	{
		bool activated = false;
		foreach (ItemScript script in this.scripts)
		{
			if (script.ScriptTrigger == trigger)
			{
				script.Effect.Execute(activator, script.Parameters);
				activated = true;
			}
		}
		return activated;
	}

}