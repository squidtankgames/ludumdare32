﻿public enum ItemType
{
	None,
	Soul,
	Ability,
	Consumable,
	Weapon
}
