﻿using Spineless;
using Spineless.Platformer;
using UnityEngine;


/// <summary>
/// Place on a gameobject to allow it to do damage.
/// </summary>
[RequireComponent(typeof(Collider2D))]
[AddComponentMenu("BacterioRage/Weapons/Injector")]
public class Injector : GameComponent
{

	#region Fields
	[SerializeField] private int attackPower;
	[SerializeField] private float attackSpeed;
	[SerializeField] private float cooldownTime;
	[SerializeField] private GameObject owner;

	private State state;
	private ActorComponent enemy;
	private bool enemyIntialColliderState;
	private BoxCollider2D enemyCollider;
	#endregion


	#region Properties
	/// <summary>
	/// Attack strength of the weapon.
	/// </summary>
	public int AttackPower
	{
		get { return this.attackPower; }
	}


	/// <summary>
	/// Scales how quickly an attack animation executes.
	/// </summary>
	public float AttackSpeed
	{
		get { return this.attackSpeed; }
	}


	/// <summary>
	/// Time (in milliseconds) before weapon attack can be executed again.
	/// </summary>
	public float CooldownTime
	{
		get { return this.cooldownTime; }
	}
	#endregion
		

	#region Enums
	private enum State
	{
		Idle,
		Injecting,
		Cooldown
	}
	#endregion


	#region Initialization
	public void Awake()
	{
		this.state = State.Idle;
		this.Renderer.enabled = false;
		this.Collider2D.enabled = false;
	}

	public void Start()
	{
		
	}
	#endregion


	#region Trigger Events
	public void OnTriggerEnter2D(Collider2D other)
	{
		Debug.Log("HIT: " + other.gameObject.name);
		this.enemy = other.gameObject.GetComponent<ActorComponent>();
		if (enemy != null)
		{
			// Play sound.
			this.Audio.Play();

			this.enemy.LastAttacker = this.owner; // this.transform.parent.gameObject;

			// Make enemy's collider solid, move us on top of it.
			this.enemyCollider = other.gameObject.GetComponent<BoxCollider2D>();
			this.enemyIntialColliderState = this.enemyCollider.isTrigger;
			this.enemyCollider.isTrigger = false;
			this.transform.parent.transform.position =
				new Vector2(other.transform.position.x, 
					other.transform.position.y + 
					this.enemyCollider.bounds.extents.y +
					this.transform.parent.GetComponent<BoxCollider2D>().bounds.extents.y
					);
			
			// Stun them.
			PhysicsBody physicsBody = this.enemy.GetComponent<PhysicsBody>();
			if (physicsBody != null)
				physicsBody.enabled = false;
			InputReceiver inputReceiver = this.enemy.GetComponent<InputReceiver>();
			if (inputReceiver != null)
				inputReceiver.enabled = false;
			
			enemy.Health -= this.AttackPower;
			
		}
	}
	#endregion


	public void Update()
	{
		if (this.state == State.Injecting)
		{
			//Debug.Log("Injecting");
		}
	}


	public virtual void Attack()
	{
		// Can only attack if cooldown timer isn't active.
		if (this.state == State.Idle)
		{
			this.Renderer.enabled = true;
			this.Collider2D.enabled = true;
			this.state = State.Injecting;
			Coroutines.Wait(this.AttackSpeed, EndAttack);
		}
	}


	public virtual void EndAttack()
	{
		Debug.Log("Inject ended.");
		this.owner.GetComponent<InputReceiver>().enabled = true;
		
		// Unstun the enemy, if one was hit.
		if (this.enemy != null)
		{
			this.enemyCollider.isTrigger = this.enemyIntialColliderState;
			PhysicsBody physicsBody = this.enemy.GetComponent<PhysicsBody>();
			if (physicsBody != null)
				physicsBody.enabled = true;
			InputReceiver inputReceiver = this.enemy.GetComponent<InputReceiver>();
			if (inputReceiver != null)
				inputReceiver.enabled = true;
			this.enemy.LastAttacker = null;
			this.enemy = null;
		}
		
		this.Renderer.enabled = false;
		this.Collider2D.enabled = false;
		this.state = State.Cooldown;
			
		Coroutines.Wait(this.CooldownTime, () =>
		{
			this.state = State.Idle;
			this.owner.GetComponent<ActorComponent>()
				.ActionState.ChangeState(ActorComponent.State.Action.Idle);
			Debug.Log("Inject ooldown ended.");
		});
	}

}
