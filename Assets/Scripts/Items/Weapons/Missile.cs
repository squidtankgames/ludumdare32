﻿using Spineless;
using UnityEngine;


[RequireComponent(typeof(Collider2D))]
[RequireComponent(typeof(Rigidbody2D))]
[AddComponentMenu("BacterioRage/Weapons/Missile")]
public class Missile : GameComponent
{

	#region Fields
	[SerializeField] private int attackPower;

	private GameObject owner;
	private ActorComponent enemy;
	#endregion


	#region Properties
	/// <summary>
	/// Attack strength of the weapon.
	/// </summary>
	public int AttackPower
	{
		get { return this.attackPower; }
	}


	/// <summary>
	/// Who fired the missile.
	/// </summary>
	public GameObject Owner
	{
		get { return owner; }
		set { owner = value; }
	}


	/// <summary>
	/// Velocity of the missile.
	/// </summary>
	public Vector2 Velocity
	{
		get { return this.Rigidbody2D.velocity; }
		set { this.Rigidbody2D.velocity = value; }
	}
	#endregion


	#region Initialization
	public void Start()
	{
		this.Rigidbody2D.gravityScale = 0;
		//this.Rigidbody2D.isKinematic = true;
	}
	#endregion


	#region Trigger Events
	public virtual void OnTriggerEnter2D(Collider2D other)
	{
		// Ignore owner's collider.
		if (other == this.owner.GetComponent<Collider2D>())
			return;

		// Hit an enemy.
		Debug.Log("MISSILE HIT: " + other.gameObject.name);
		this.enemy = other.gameObject.GetComponent<ActorComponent>();
		if (enemy != null)
		{
			this.Audio.Play();
			enemy.Health -= this.AttackPower;
			//TODO: Some sort of hit animation.
			this.GameObject.Destroy();
		}
		
		// Hit something solid at least, just blow up.
		else if (!other.isTrigger)
		{
			this.GameObject.Destroy();
		}
	}
	#endregion

}

