﻿using System;
using System.Collections.Generic;

using Spineless;
using UnityEngine;


/// <summary>
/// Represents a collection of Items possessed and/or equipped by an entity.
/// </summary>
[Serializable]
public class Inventory :
	ICloneable
{

	#region Fields
	[SerializeField] private int maxItemRecords;
	[SerializeField] private ItemRecord equippedWeapon;
	[SerializeField] private ItemRecord equippedArmor;
	[SerializeField] private List<ItemRecord> equippedCharms;
	[SerializeField] private List<ItemRecord> items;

	private ActorComponent owner;
	#endregion


	#region Properties
	public ItemRecord EquippedArmor
	{
		get { return this.equippedArmor; }
	}

	public List<ItemRecord> EquippedCharms
	{
		get { return this.equippedCharms; }
	}

	public ItemRecord EquippedWeapon
	{
		get { return this.equippedWeapon; }
	}

	public List<ItemRecord> Items
	{
		get { return this.items; }
	}

	public ActorComponent Owner
	{
		get { return this.owner; }
	}

	#endregion


	#region Indexers
	public ItemRecord this[int index]
	{
		get { return this.items[index]; }
	}
	#endregion


	#region ICloneable
	public object Clone()
	{
		Inventory clone = (Inventory) this.MemberwiseClone();
		// TODO: Proper clone of list.
		return clone;
	}
	#endregion


	/// <summary>
	/// Add an item instance to inventory.
	/// Raises respective events on any item properties.
	/// </summary>
	/// <param name="acquiredItem">Item instance to be added.</param>
	/// <returns>Quantity of the item successfully added to inventory</returns>
	public int AddItem(ItemRecord acquiredItem)
	{
		int amountAdded = 0;
		foreach (ItemRecord inventoryItem in this.items)
		{
			// Inventory already has a stack of this item, just increment its quantity.
			if (inventoryItem.ItemId == acquiredItem.ItemId)
			{
				amountAdded = inventoryItem.IncrementQuantity(acquiredItem.Quantity);
			}

			// No previous stack found, clone the record itself.
			else if (this.items.Count < this.maxItemRecords)
			{
				this.items.Add((ItemRecord) acquiredItem.Clone());
				amountAdded = acquiredItem.Quantity;
			}
		}
			
		return amountAdded;
	}


	/// <summary>
	/// Equip an item as a weapon.
	/// Only succeeds if item exists in Inventory and is actually a weapon.
	/// </summary>
	/// <param name="weapon">Item to equip as weapon.</param>
	/// <returns>True if item was successfully equipped.</returns>
	public bool EquipWeapon(ItemRecord weapon)
	{
		// Inventory isn't attached to any Actor, so can't actually equip anything.
		if (this.owner == null)
		{
			Debug.LogError("Inventory must be owned by an Actor to equip a weapon.");
			return false;
		}
				

		if (this.items.Contains(weapon))
		{
			// Unequip previous weapon, equip new one, and activate any associated scripts.
			if (weapon.Data.Type == ItemType.Weapon)
			{
				this.equippedWeapon.Data.ActivateScript(ItemScript.Trigger.OnUnequipped, this.owner.GameObject);
				this.equippedWeapon = weapon;
				this.equippedWeapon.Data.ActivateScript(ItemScript.Trigger.OnEquipped, this.owner.GameObject);
				return true;
			}
			else
			{
				Debug.LogWarning("EquipWeapon: " + weapon.Data.Name + " is not a weapon.");
				return false;
			}
		}
		else
		{
			Debug.LogWarning("EquipWeapon: " + weapon.Data.Name + " is not in " + this.owner.Data.Name + "'s Inventory.");
			return false;
		}
	}


	//TODO: DropItem() removes item from inventory, instantiates it in scene, returns it.
	//TODO: TrashItem() removes item from inventory, and Destroys it.

}