﻿using Spineless;


public class WeaponProperties
{

	#region Fields
	private int attackPower;
	private float attackSpeed;
	private float cooldownTime;
	#endregion


	#region Constructors
	public WeaponProperties(StringDictionary properties)
	{
		this.attackPower = int.Parse(properties["AttackPower"]);
		this.attackSpeed = float.Parse(properties["AttackSpeed"]);
		this.cooldownTime = float.Parse(properties["CooldownTime"]);
	}
	#endregion


	#region Properties
	/// <summary>
	/// Attack strength of the weapon.
	/// </summary>
	public int AttackPower
	{
		get { return this.attackPower; }
	}


	/// <summary>
	/// Scales how quickly an attack animation executes.
	/// </summary>
	public float AttackSpeed
	{
		get { return this.attackSpeed; }
	}


	/// <summary>
	/// Time (in milliseconds) before weapon attack can be executed again.
	/// </summary>
	public float CooldownTime
	{
		get { return this.cooldownTime; }
	}
	#endregion

}