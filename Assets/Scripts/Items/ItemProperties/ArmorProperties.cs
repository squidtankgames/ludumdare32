﻿using Spineless;


public class ArmorProperties
{

	#region Fields
	private int defense;
	#endregion


	#region Constructors
	public ArmorProperties(StringDictionary properties)
	{
		this.defense = int.Parse(properties["Defense"]);
	}
	#endregion


	#region Properties
	/// <summary>
	/// Defense value of the armor item.
	/// </summary>
	public int Defense
	{
		get { return this.defense; }
	}
	#endregion

}