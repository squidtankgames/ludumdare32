﻿using UnityEditor;
using UnityEngine;


public class CreateDataAssets
{


	[MenuItem("Assets/Create/BacterioRage/Actor Database")]
	public static void CreateActorDB()
	{
		CreateAsset<ActorDB>("Assets/ActorDB.asset");
	}


	[MenuItem("Assets/Create/BacterioRage/Item Database")]
	public static void CreateItemData()
	{
		CreateAsset<ItemDB>("Assets/ItemDB.asset");
	}


	[MenuItem("Assets/Create/BacterioRage/StaticData")]
	public static void CreateStaticData()
	{
		CreateAsset<StaticData>("Assets/StaticData.asset");
	}


	#region Helpers
	private static void CreateAsset<T>(string assetPath) where T : ScriptableObject
	{
		var asset = ScriptableObject.CreateInstance<T>();

		AssetDatabase.CreateAsset(asset, assetPath);
		AssetDatabase.SaveAssets();
		EditorUtility.FocusProjectWindow();
		Selection.activeObject = asset;
	}
	#endregion

}