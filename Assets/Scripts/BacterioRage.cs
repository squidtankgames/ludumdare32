﻿using System.Collections.Generic;

using Spineless;
using UnityEngine;
using UnityEngine.UI;


[AddComponentMenu("BacterioRage/Main Game Component")]
public class BacterioRage : GameComponent
{

	#region Fields

	public static GameObject SplashScreen;
	public static GameObject LevelSelectScreen;
	public static PlayerHUD PlayerHUD;
	public static NotificationScreen Notification;

	private static BacterioRage game;
	private GameObject player;
	private bool firstRun = true;
	
	[SerializeField] private StaticData staticData;
	[SerializeField] private InputSource playerInput;
	[SerializeField] private List<string> gameFlags;
	#endregion


	#region Properties
	/// <summary>
	/// Global access to the main game object across scenes.
	/// </summary>
	public static BacterioRage Game
	{
		get { return game; }
	}


	/// <summary>
	/// Indicators that special game events have taken place.
	/// </summary>
	public List<string> GameFlags
	{
		get { return gameFlags; }
	}


	/// <summary>
	/// The player's GameObject.
	/// </summary>
	public GameObject Player
	{
		get { return player; }
		set { player = value; }
	}


	/// <summary>
	/// Input Source receiving input from player peripherals.
	/// </summary>
	public InputSource PlayerInput
	{
		get { return this.playerInput; }
	}


	/// <summary>
	/// Static data resources for the game.
	/// </summary>
	public StaticData StaticData
	{
		get { return this.staticData; }
	}
	#endregion


	#region Initialization
	public void Awake()
	{
		if (game)
			Destroy(gameObject);
		else
		{
			game = this;
			DontDestroyOnLoad(this.gameObject);
			firstRun = true;
			Debug.Log("Instantiated BacterioRage object.");
		}
	}


	public void Start()
	{
		Debug.Log("Scene loaded.");
		ConfigureCamera(Camera.main);
		GrabComponents();
		this.Updateables.AddUpdateable(this.playerInput);
		ShowSplashScreen();
	}
	#endregion


	#region Updates
	public void Update()
	{
		ProcessUpdates();
	}

		
	public void FixedUpdate()
	{
		ProcessFixedUpdates();
	}
	#endregion


	/// <summary>
	/// Returns whether a game flag has been set.
	/// </summary>
	/// <param name="gameFlag">Flag to check for.</param>
	/// <returns>True if flag was found.</returns>
	public bool IsFlagSet(string gameFlag)
	{
		foreach (string flag in this.gameFlags)
		{
			if (flag.Equals(gameFlag))
				return true;
		}

		return false;
	}


	/// <summary>
	/// Pause/Unpause the game.
	/// </summary>
	/// <param name="pause"></param>
	public void SetPaused(bool pause)
	{
		if (pause)
		{
			Time.timeScale = 0;
			Debug.Log("Paused");
		}

		else
		{
			Time.timeScale = 1;
			Debug.Log("Unpaused");
		}
			
	}


	public void ShowSplashScreen()
	{
		SetPaused(true);
		SplashScreen.transform.GetChild(0).gameObject.SetActive(true);
	}


	public void ShowLevelSelect()
	{
		//levels.staphStart.SetActive(false);
		//levels.salmonellaStart.SetActive(false);
		//levels.meningitisStart.SetActive(false);
		//levels.tbStart.SetActive(false);
		LevelSelectScreen.transform.GetChild(0).gameObject.SetActive(true);
		LevelSelectScreen levels = LevelSelectScreen.GetComponent<LevelSelectScreen>();
		levels.BeginLevelSelect();	
	}

	
	/// <summary>
	/// Reload the game.
	/// Should still keep current progress.
	/// </summary>
	public void Reload()
	{
		Debug.Log("Reloading scene.");
		Application.LoadLevel(Application.loadedLevel);
	}


	private void GrabComponents()
	{
		Debug.Log("Grabbing Screens");
		LevelSelectScreen = GameObject.FindGameObjectWithTag("LevelSelectScreen");
		SplashScreen = GameObject.FindGameObjectWithTag("SplashScreen");
		PlayerHUD = GameObject.FindGameObjectWithTag("PlayerHUD").GetComponent<PlayerHUD>();
		Notification = GameObject.FindGameObjectWithTag("NotificationScreen").GetComponent<NotificationScreen>();
	}


	private void ConfigureCamera(Camera camera)
	{
		camera.orthographicSize = 540f;

		// Uncomment below for pixel-perfect, uncomment below.
		// Keep in mind, screen height is based on game window size,
		// not monitor resolution. 
		// So, if all graphics were designed for 1080, then size should
		// always be 540.
		//camera.orthographicSize = Screen.height/2f;
	}

}
