﻿using System.Collections.Generic;

using UnityEngine;


public class ActorDB : ScriptableObject
{

	#region Fields

	[SerializeField]
	private List<ActorData> actors;
	#endregion


	#region Properties
	public List<ActorData> Actors
	{
		get { return this.actors; }
	}
	#endregion


	#region Indexers
	/// <summary>
	/// Retrieve an actor by its unique ID.
	/// </summary>
	/// <param name="index">Numeric ID of the actor.</param>
	/// <returns>An Actor with the ID assigned.</returns>
	public ActorData this[int index]
	{
		get
		{
			return this.actors[index];
		}
	}
	#endregion


	public ActorData GetById(int actorId)
	{
		for (int i = 0; i < this.actors.Count; i++)
		{
			if (this.actors[i].Id == actorId)
				return this.actors[i];
		}

		Debug.Log("ActorDB: Could not find actor with ID " + actorId);

		return null;
	}

}