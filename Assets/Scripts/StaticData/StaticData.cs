﻿using Spineless;
using UnityEngine;


public class StaticData : ScriptableObject
{

	#region Fields
	[SerializeField]
	private ActorDB actors;
	[SerializeField]
	private ItemDB items;
	[SerializeField]
	private InputSource defaultPlatformingControls;
	[SerializeField]
	private InputSource defaultMenuControls;
	#endregion


	#region Properties
	/// <summary>
	/// Static data for all actors.
	/// </summary>
	public ActorDB Actors
	{
		get { return this.actors; }
	}


	/// <summary>
	/// Default menu controls in the absense of player-specified button configuration.
	/// </summary>
	public InputSource DefaultMenuControls
	{
		get { return this.defaultMenuControls; }
	}


	/// <summary>
	/// Default platforming controls in the absense of player-specified button configuration.
	/// </summary>
	public InputSource DefaultPlatformingControls
	{
		get { return this.defaultPlatformingControls; }
	}


	/// <summary>
	/// Static data for all items.
	/// </summary>
	public ItemDB Items
	{
		get { return this.items; }
	}
	#endregion

}