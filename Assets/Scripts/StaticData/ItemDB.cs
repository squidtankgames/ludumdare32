﻿using System.Collections.Generic;

using UnityEngine;


public class ItemDB : ScriptableObject
{

	#region Fields
	[SerializeField]
	private List<ItemData> items;
	#endregion


	#region Properties

	#endregion


	#region Indexers
	/// <summary>
	///  Retrieve an item by its unique ID.
	/// </summary>
	/// <param name="index">Numeric ID of the item.</param>
	/// <returns>An ItemComponent with the ID assigned.</returns>
	public ItemData this[int index]
	{
		get
		{
			return this.items[index];
		}
	}
	#endregion


	/// <summary>
	/// Retrieve an item's data according to its Item ID.
	/// </summary>
	/// <param name="itemID">Id of the item data to retrieve.</param>
	/// <returns>Data for the iteam.</returns>
	public ItemData GetByID(int itemID)
	{
		for (int i = 0; i < this.items.Count; i++)
		{
			if (this.items[i].Id == itemID)
				return this.items[i];
		}

		return null;
	}

}