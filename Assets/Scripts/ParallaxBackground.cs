﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Grabs all game objects tagged as "Background"
/// and scrolls them based on distance from the camera.
/// 
/// Adapted from the Unity 2D game demo project.
/// 
/// The closest background layer to the camera must have
/// a Z position > 0. Going away from the camera in
/// increments of 1 works fairly well.
/// </summary>
public class ParallaxBackground : MonoBehaviour
{

	public GameObject[] backgrounds; // Array of all the backgrounds to be parallaxed.
	public float parallaxScale = 0.8f; // The proportion of the camera's movement to move the backgrounds by.
	public float parallaxReductionFactor = 0.4f; // How much less each successive layer should parallax.
	public float smoothing = 8.0f; // How smooth the parallax effect should be.

	private Vector3 previousCamPosition; // The postion of the camera in the previous frame.

	public void Start()
	{
		this.previousCamPosition = Camera.main.transform.position;

		backgrounds = GameObject.FindGameObjectsWithTag("Background");

		Debug.Log("Found " + backgrounds.Length + " parallax background layer(s)");
	}

	public void Update()
	{
		// The parallax is the opposite of the camera movement since the previous frame multiplied by the scale.
		float parallax = ((Camera.main.transform.position.x - previousCamPosition.x) * parallaxScale);

		for(int i = 0; i < backgrounds.Length; i++)
		{
			float backgroundZ = backgrounds[i].transform.position.z;

			// Set a target x position which is their current position plus the parallax multiplied by the reduction.
			float backgroundTargetX = backgrounds[i].transform.position.x + parallax * (backgroundZ * parallaxReductionFactor);

			// Create a target position which is the background's current position but with its target x position.
			Vector3 backgroundTargetPosition = new Vector3(backgroundTargetX, backgrounds[i].transform.position.y, backgrounds[i].transform.position.z);
			
			// Lerp the background's position between itself and its target position.
			backgrounds[i].transform.position = Vector3.Lerp(backgrounds[i].transform.position, backgroundTargetPosition, smoothing * Time.deltaTime);
		}

		previousCamPosition = Camera.main.transform.position;
	}
}
