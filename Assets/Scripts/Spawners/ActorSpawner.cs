﻿using System.Collections.Generic;

using Spineless;
using Spineless.Platformer;
using UnityEngine;


[AddComponentMenu("BacterioRage/Spawners/Actor Spawner")]
public class ActorSpawner : GameComponent
{

	#region Fields
	[SerializeField] int actorId = 0; // TODO: Change this to enum of actors.
	[SerializeField] bool isPlayer = false;
	[SerializeField] int maxInstances = 1;
	[SerializeField] int instancesToSpawn = 1;
	[SerializeField] float interval = 0;
	[SerializeField] private AudioClip music;

	private List<GameObject> activeInstances;
	private ActorData actorData;
	private Coroutine spawnTimer;
	private int totalInstances;
	#endregion
		

	#region Properties
	public Coroutine SpawnTimer
	{
		get { return this.spawnTimer; }
	}


	public List<GameObject> ActiveInstances
	{
		get { return this.activeInstances; }
	}
	#endregion


	#region Initialization
	public void Awake()
	{
		this.activeInstances = new List<GameObject>();			
			
	}

	
	public void Start()
	{
		this.actorData = BacterioRage.Game.StaticData.Actors.GetById(this.actorId);
		StartSpawnTimer();
	}
	#endregion


	#region Updates
	public void Update()
	{
		// Remove destroyed actors from activeInstances collection.
		for (int i = 0; i < this.activeInstances.Count; i++)
		{
			if (this.activeInstances[i] == null)
				this.activeInstances.RemoveAt(i);		
		}

		// If timer was stopped, and more instances can be spawned, start the timer again.
		if (this.spawnTimer == null)
		{
			if (this.activeInstances.Count < this.maxInstances)
				StartSpawnTimer();
		}
	}
	#endregion


	public void Spawn()
	{
		for (int i = 0; i < this.instancesToSpawn; i++)
		{
			// Max instances reached, stop timer and abort.
			if (this.activeInstances.Count >= this.maxInstances)
			{
				StopSpawnTimer();
				return;
			}
					
				
			// Create new actor and add it to activeInstances collection.
			GameObject newInstance = this.actorData.Instantiate(this.Transform);
			newInstance.gameObject.name = this.actorData.Name;
			this.activeInstances.Add(newInstance);
			this.totalInstances++;

			// Set up spawned actor as the player.
			if (this.isPlayer)
			{
				AudioSource audio = Camera.main.GetComponent<AudioSource>();
				if (audio.clip != music)
				{
					audio.clip = music;
					audio.Play();
				}

				// Set player input to spawned instance.
				ActorComponent player = newInstance.GetComponent<ActorComponent>();

				InputReceiver actorInput = newInstance.GetComponent<InputReceiver>();
				if (actorInput != null)
					actorInput.InputSource = BacterioRage.Game.PlayerInput;

				CameraFollow camera = Camera.main.GetComponent<CameraFollow>();
				camera.player = newInstance.transform;
				BacterioRage.Game.Player = newInstance;
				BacterioRage.PlayerHUD.Player = newInstance;

				// Grant additional powers based on game flags.
				if (BacterioRage.Game.IsFlagSet("DefeatedStaph"))
				{
					// More health.
					player.Health.Maximum = 200;
					player.Health.Current = 200;
				}
				if (BacterioRage.Game.IsFlagSet("DefeatedSalmonella"))
				{
					// Spine attack.
					player.GetComponent<RangedAttack>().enabled = true;
				}
				if (BacterioRage.Game.IsFlagSet("DefeatedTB"))
				{
					// Jump higher.
					player.GetComponent<ActorJump>().JumpPower += 20;
				}
				if (BacterioRage.Game.IsFlagSet("DefeatedMeningitis"))
				{
					// Double jump.
					player.GetComponent<ActorDash>().enabled = true;
				}

			}
					
		}

		// Restart timer.
		StartSpawnTimer();
	}


	public void StartSpawnTimer()
	{
		this.spawnTimer = Coroutines.Wait(this.interval, Spawn);
	}



	public void StopSpawnTimer()
	{
		if (this.spawnTimer != null)
		{
			StopCoroutine(this.spawnTimer);
			this.spawnTimer = null;
		}
	}

}
