﻿using Spineless;
using UnityEngine;
using UnityEngine.UI;

public class LevelSelectScreen : GameComponent
{
	public AudioClip music;

	public Button staph;
	public Button salmonella;
	public Button tb;
	public Button meningitis;

	public GameObject staphStart;
	public GameObject salmonellaStart;
	public GameObject tbStart;
	public GameObject meningitisStart;

	public Color disabledColor;
	public Color enabledColor;

	public static GameObject instance;


	public void Awake()
	{
		/*
		if (instance)
			Destroy(gameObject);
		else
		{
			instance = this.GameObject;
			DontDestroyOnLoad(this.gameObject);
		}
		 */
	}


	public void BeginLevelSelect()
	{
		AudioSource audio = Camera.main.GetComponent<AudioSource>();
		audio.clip = music;
		audio.Play();

		BacterioRage.Game.SetPaused(true);

		Debug.Log("Level Select Screen");
		staphStart.SetActive(false);
		salmonellaStart.SetActive(false);
		tbStart.SetActive(false);
		meningitisStart.SetActive(false);

		

		if (BacterioRage.Game.IsFlagSet("DefeatedStaph"))
			staph.interactable = false;
		else
			staph.interactable = true;

		if (BacterioRage.Game.IsFlagSet("DefeatedSalmonella"))
			salmonella.interactable = false;
		else
			salmonella.interactable = true;

		if (BacterioRage.Game.IsFlagSet("DefeatedTB"))
			tb.interactable = false;
		else
			tb.interactable = true;

		if (BacterioRage.Game.IsFlagSet("DefeatedMeningitis"))
			meningitis.interactable = false;
		else
			meningitis.interactable = true;
	}


	public void LoadLevel(string level)
	{
		Debug.Log("LEVEL SELECTED");
		switch (level)
		{
			case "Staph":
				staphStart.SetActive(true);
				BacterioRage.Game.SetPaused(false);
				this.Transform.GetChild(0).gameObject.SetActive(false);
				break;
			case "Salmonella":
				salmonellaStart.SetActive(true);
				BacterioRage.Game.SetPaused(false);
				this.Transform.GetChild(0).gameObject.SetActive(false);
				break;
			case "TB":
				tbStart.SetActive(true);
				BacterioRage.Game.SetPaused(false);
				this.Transform.GetChild(0).gameObject.SetActive(false);
				break;
			case "Meningitis":
				meningitisStart.SetActive(true);
				BacterioRage.Game.SetPaused(false);
				this.Transform.GetChild(0).gameObject.SetActive(false);
				break;
			default:
				Debug.Log(level + "is not a valid level.");
				break;
		}
	}

}

