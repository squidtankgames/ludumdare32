﻿using Spineless;
using UnityEngine;
using UnityEngine.UI;

public class NotificationScreen : GameComponent
{

	public Text text;
	private System.Action callback;
	public static GameObject instance;

	public void Awake()
	{
		/*
		if (instance)
			Destroy(gameObject);
		else
		{
			instance = this.GameObject;
			DontDestroyOnLoad(this.gameObject);
		}
		 */
	}

	public void Proceed()
	{
		Debug.Log("Closed notification.");
		BacterioRage.Game.SetPaused(false);
		//BacterioRage.Game.ShowLevelSelect();
		if (callback != null)
		{
			Debug.Log("Raising callback.");
			callback.Raise();
		}
			
		this.Transform.GetChild(0).gameObject.SetActive(false);	
		BacterioRage.Game.ShowLevelSelect();
	}


	public void ShowMessage(string text, System.Action callback)
	{
		BacterioRage.Game.SetPaused(true);
		this.text.text = text;
		this.callback = callback;
	}

}

