﻿using UnityEngine;
using System.Collections;

public class SortLayer : MonoBehaviour
{
	public string SortingLayer = "GUI";

	// Use this for initialization
	public void Start()
	{
		this.GetComponent<Renderer>().sortingLayerName = SortingLayer;
	}
}
