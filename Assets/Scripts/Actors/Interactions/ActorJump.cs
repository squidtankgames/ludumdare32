﻿using Spineless;
using Spineless.Platformer;
using UnityEngine;


[AddComponentMenu("BacterioRage/Interactions/Jump")]
public class ActorJump : Jump
{

	#region Fields
	private ActorComponent actorComponent;
	#endregion


	#region Initialization
	public override void Start()
	{
		base.Start();
			
		this.actorComponent = this.GameObject.GetComponent<ActorComponent>();
		if (this.actorComponent == null)
			Debug.LogError("GameObject with BacterioRage.Walk component has no Actor component!");
	}

	#endregion


	#region Event Handlers
	public override void OnTouchedGround()
	{
		base.OnTouchedGround();
		this.actorComponent.JumpState.ChangeState(ActorComponent.State.Jump.Ground);
	}
	#endregion


	#region Updates
	public void Update()
	{
		// Check for falling.
		if (!this.physicsBody.Grounded)
		{
			switch (this.physicsBody.GroundOrientation)
			{
				case Direction.Down:
					if (this.physicsBody.Velocity.y.Sign() != 1)
						Fall();
					break;
				case Direction.Up:
					if (this.physicsBody.Velocity.y.Sign() != -1)
						Fall();
					break;
				case Direction.Left:
					if (this.physicsBody.Velocity.x.Sign() != 1)
						Fall();
					break;
				case Direction.Right:
					if (this.physicsBody.Velocity.x.Sign() != -1)
						Fall();
					break;
			}
		}
	}
	#endregion


	public override void InitiateJump()
	{
		base.InitiateJump();
		this.actorComponent.JumpState.ChangeState(ActorComponent.State.Jump.Rise);
	} 


	public void Fall()
	{
		this.actorComponent.JumpState.ChangeState(ActorComponent.State.Jump.Fall);
	}
}