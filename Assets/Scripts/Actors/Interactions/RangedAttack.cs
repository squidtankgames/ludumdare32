﻿using System;
using Spineless;
using Spineless.Platformer;
using UnityEngine;

[AddComponentMenu("BacterioRage/Interactions/Ranged Attack")]
[RequireComponent(typeof(InputReceiver))]
public class RangedAttack : GameComponent
{

	#region Fields
	[SerializeField] private Missile missilePrefab;
	[SerializeField] private float travelSpeed;
	[SerializeField] private bool targetsPlayer;

	private ActorComponent actorComponent;
	private InputReceiver inputReceiver;
	private bool attackActive;
	#endregion


	#region Properties
	public bool CanAttack
	{
		get { return (!attackActive); }
	}


	public static Vector3 MousePosition
	{
		get
		{
			return Camera.main.ScreenToWorldPoint(Input.mousePosition); 
		}
	}
	#endregion


	#region Initialization
	public void Awake()
	{
		this.attackActive = false;
	}


	public void Start()
	{

		this.inputReceiver = GameObject.GetComponent<InputReceiver>();
		this.actorComponent = this.GameObject.GetComponent<ActorComponent>();

		if (this.inputReceiver != null)
		{
			this.inputReceiver.InputStarted += OnInputStarted;
			this.inputReceiver.InputEnded += OnInputEnded;
		}
		else
			Debug.LogError("Attack requires a BasicInputReceiver component exist on the same GameObject.");
	}
	#endregion


	#region InputReceiver Event Handlers
	/// <summary>
	/// Attack input was received.
	/// </summary>
	/// <param name="command">Possible Attack command.</param>
	public void OnInputStarted(string command)
	{
		if (command.Equals(Commands.Attack))
		{
			if (this.actorComponent.ActionState.CurrentState == ActorComponent.State.Action.Idle)
				InitiateAttack();
		}
	}


	/// <summary>
	/// Attack input was ended.
	/// </summary>
	/// <param name="command">Possible Attack command.</param>
	public void OnInputEnded(string command)
	{
		if (command.Equals(Commands.Attack))
		{
			TerminateAttack();
		}
	}
	#endregion


	public void InitiateAttack()
	{
		if (this.targetsPlayer)
		{
			if (BacterioRage.Game.Player == null)
				return;

			Vector2 playerPos = BacterioRage.Game.Player.transform.position.Absolute();
			Vector2 enemyPos = this.Transform.position.Absolute();
			if (Math.Abs(enemyPos.x - playerPos.x) > 1000)
				return;
		}

		this.attackActive = true;
		//this.actorComponent.ActionState.ChangeState(ActorComponent.State.Action.Attack);

		Missile missile = this.missilePrefab.GameObject.Instantiate().GetComponent<Missile>();
		missile.Transform.position = this.Transform.position;

		if (this.targetsPlayer)
		{
			missile.GameObject.RotateToward(BacterioRage.Game.Player.transform.position);
		}	
		else
			missile.GameObject.RotateToward(MousePosition);
		
		missile.Rigidbody2D.AddForce(missile.Transform.up * this.travelSpeed);
		missile.Owner = this.GameObject;
	}


	public void TerminateAttack()
	{
		this.attackActive = false;
		//this.actorComponent.ActionState.ChangeState(ActorComponent.State.Action.Idle);
	}


	



}

