﻿using Spineless;
using Spineless.Platformer;
using UnityEngine;


[AddComponentMenu("BacterioRage/Interactions/Walk")]
public class ActorWalk : Walk
{

	#region Fields
	private ActorComponent actorComponent;
	#endregion


	#region Initialization
	public override void Start()
	{
		base.Start();
		this.actorComponent = this.GameObject.GetComponent<ActorComponent>();
	}
	#endregion


	public override void MoveLeft()
	{
		// Flip sprite if changing direction.
		if (this.moveForce == null)
		{
			if (this.Transform.localScale.x.Sign() != -1)
				this.Transform.localScale = this.Transform.localScale.SetX(Transform.localScale.x * -1);	
		}
			
		base.MoveLeft();
		this.actorComponent.MobileState.ChangeState(ActorComponent.State.Mobile.Walk);
	}


	public override void MoveRight()
	{
		// Flip sprite if changing direction.
		if (this.moveForce == null)
		{
			if (Transform.localScale.x.Sign() != 1)
				Transform.localScale = Transform.localScale.SetX(Transform.localScale.x * -1);
		}

		base.MoveRight();
		this.actorComponent.MobileState.ChangeState(ActorComponent.State.Mobile.Walk);
	}


	public override void TerminateMove()
	{
		base.TerminateMove();

		this.actorComponent.MobileState.ChangeState(ActorComponent.State.Mobile.Stationary);
	}
}
