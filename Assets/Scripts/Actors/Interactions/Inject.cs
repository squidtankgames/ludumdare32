﻿using Spineless;
using Spineless.Platformer;
using UnityEngine;


[AddComponentMenu("BacterioRage/Interactions/Inject")]
[RequireComponent(typeof(InputReceiver))]
public class Inject : GameComponent
{

	#region Fields
	[SerializeField]
	private Injector injector;

	private ActorComponent actorComponent;
	private InputReceiver inputReceiver;
	private bool injectActive;
	#endregion
		
		
	#region Properties
	public bool CanAttack
	{
		get { return (!injectActive); }
	}
	#endregion


	#region Initialization
	public void Awake()
	{
		this.injectActive = false;
	}


	public void Start()
	{
			
		this.inputReceiver = GameObject.GetComponent<InputReceiver>();
		this.actorComponent = this.GameObject.GetComponent<ActorComponent>();

		if (this.inputReceiver != null)
		{
			this.inputReceiver.InputStarted += OnInputStarted;
			this.inputReceiver.InputEnded += OnInputEnded;
		}
		else
			Debug.LogError("Inject requires a BasicInputReceiver component exist on the same GameObject.");
	}
	#endregion


	#region InputReceiver Event Handlers
	/// <summary>
	/// Inject input was received.
	/// </summary>
	/// <param name="command">Possible Inject command.</param>
	public void OnInputStarted(string command)
	{
		if (command.Equals(Commands.Inject))
		{
			if (this.actorComponent.MobileState.CurrentState == ActorComponent.State.Mobile.Stationary
				&& this.actorComponent.JumpState.CurrentState == ActorComponent.State.Jump.Ground)
			InitiateInject();
		}
	}


	/// <summary>
	/// Inject input was ended.
	/// </summary>
	/// <param name="command">Possible Inject command.</param>
	public void OnInputEnded(string command)
	{
		
	}
	#endregion


	public void InitiateInject()
	{
		this.inputReceiver.enabled = false;
		this.injectActive = true;
		this.actorComponent.ActionState.ChangeState(ActorComponent.State.Action.Attack);
		injector.Attack();
	}


	public void TerminateInject()
	{
		this.injectActive = false;
		this.actorComponent.ActionState.ChangeState(ActorComponent.State.Action.Idle);
	}
}