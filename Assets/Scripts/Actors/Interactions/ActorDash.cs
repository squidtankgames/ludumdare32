﻿using Spineless;
using Spineless.Platformer;
using UnityEngine;


[AddComponentMenu("BacterioRage/Interactions/Dash")]
public class ActorDash : Dash
{

	#region Fields
	private ActorComponent actorComponent;
	#endregion


	#region Initialization
	public override void Start()
	{
		base.Start();
		this.actorComponent = this.GameObject.GetComponent<ActorComponent>();
	}
	#endregion


	public override void ApplyDash()
	{
		base.ApplyDash();
			
		this.actorComponent.MobileState.ChangeState(ActorComponent.State.Mobile.Dash);
	}

	public override void TerminateDash()
	{
		base.TerminateDash();
		if (this.actorComponent.MobileState.CurrentState == ActorComponent.State.Mobile.Dash)
			this.actorComponent.MobileState.ChangeState(this.actorComponent.MobileState.PreviousState);
	}
}
