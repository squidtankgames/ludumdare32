﻿using System.Collections.Generic;

using Spineless;
using UnityEngine;


/// <summary>
/// Component that stores mutable data about an Actor in the scene.
/// </summary>
[AddComponentMenu("BacterioRage/Actor")]
public partial class ActorComponent : GameComponent
{

	#region Fields
	[SerializeField] private ActorData data;
	[SerializeField] private IntegerRange health;
	[SerializeField] private IntegerRange shards;
	[SerializeField] private Inventory inventory;
	[SerializeField] private List<DeathScript> deathActions = new List<DeathScript>();

	private GameObject lastAttacker;
	#endregion


	#region Properties
	public ActorData Data
	{
		get { return this.data; }
		set { this.data = value; }
	}


	public List<DeathScript> DeathActions
	{
		get { return this.deathActions; }
		set { this.deathActions = value; }
	}


	public IntegerRange Health
	{
		get { return this.health; }
		set { this.health = value; }
	}


	public GameObject LastAttacker
	{
		get { return lastAttacker; }
		set { lastAttacker = value; }
	}


	public Inventory Inventory
	{
		get { return this.inventory; }
		set { this.inventory = value; }
	}


	public IntegerRange Shards
	{
		get { return this.shards; }
		set { this.shards = value; }
	}
	#endregion


	#region Initialization
	public void Start()
	{
		this.health.ValueChanged += OnHealthChanged;
		InitializeStates();
	}
	#endregion


	#region Event Handlers
	public void OnHealthChanged(int value)
	{
		// Actor has "died."
		if (value <= this.health.Minimum)
		{
			foreach (DeathScript script in this.deathActions)
			{
				script.Execute(this.GameObject);	
			}
		}
	}
	#endregion

}
