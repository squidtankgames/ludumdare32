﻿
using Spineless;
using UnityEngine;


public class ContactDamage : GameComponent
{

	#region Fields
	[SerializeField] private int damage;
	#endregion


	public void OnTriggerEnter2D(Collider2D collider)
	{
		if (collider == this.GetComponent<Collider2D>())
			return;

		ActorComponent actor = collider.GetComponent<ActorComponent>();
		if (actor != null)
		{
			if (actor.GameObject.name.Contains(this.GameObject.name)
			    || actor.GameObject.name.Contains(this.GameObject.name + "Child")
			    || actor.GameObject.name.Contains(this.GameObject.name + "Parent"))
				return;
			
			actor.Health.Current -= damage;
		}
	}

}
