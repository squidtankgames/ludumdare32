﻿using System;

using Spineless;
using UnityEngine;


/// <summary>
/// Collects items and deposits them into an Inventory collection.
/// </summary>
public class ItemCollector : GameComponent
{

	#region Fields
	[SerializeField] private Collider2D collectorField;
	[SerializeField] private ItemType itemFilter = ItemType.None;
	[SerializeField] private Inventory targetInventory;
	#endregion


	#region Events
	/// <summary>
	/// Raised when an item is collected.
	/// Passes which item was collected from the scene, and how much of it.
	/// </summary>
	public event Action<ItemRecord, int> ItemCollected;
	#endregion


	#region Properties
	public Collider2D CollectorField
	{
		get { return this.collectorField; }
	}


	public ItemType ItemFilter
	{
		get { return this.itemFilter; }
	}


	public Inventory TargetInventory
	{
		get { return this.targetInventory; }
	}
	#endregion


	#region Collision Event Handlers
	/// <summary>
	/// Sent each frame where a collider on another object is touching this object's trigger collider.
	/// </summary>
	/// <param name="collision"></param>
	public void OnTriggerEnter2D(Collider2D collision)
	{
		if (!this.enabled)
			return;

		ItemComponent item = collision.gameObject.GetComponent<ItemComponent>();
		if (item != null)
		{
			ItemType itemType = item.Record.Data.Type;
			if (itemType == itemFilter || itemType == ItemType.None)
			{
				Debug.Log("Collector got an item!");
				int collectedAmount;

				// Collecting item into inventory.
				if (this.targetInventory != null)
					collectedAmount = item.Record.DecrementQuantity(this.targetInventory.AddItem(item.Record));

				// Not targeting an inventory, so just collect the whole thing.
				else
				{
					collectedAmount = item.Record.Quantity;
					item.Record.DecrementQuantity(item.Record.Quantity);
				}

				// Item was collected, partially or completely.
				if (collectedAmount > 0)
				{
					ItemCollected.Raise(item.Record, collectedAmount);
					item.Record.Data
						.ActivateScript(ItemScript.Trigger.OnCollected, this.targetInventory.Owner);
				}
						

				// Item depleted, so remove it from the scene.
				if (item.Record.Quantity == 0)
				{
					//TODO: May need to delay by 1 frame.
					Debug.Log("Destroyed object!");
					item.Record.SceneInstance = null;
					item.GameObject.Destroy();
				}
			}
		}
	}
	#endregion


	#region Builder
	public class Builder
	{
		private GameObject parent;
		private Collider2D collectorField;
		private ItemType itemFilter = ItemType.None;
		private Inventory targetInventory = null;

		/// <summary>
		/// Create a new ItemCollector parented under an existing GameObject.
		/// </summary>
		/// <param name="parent">GameObject the ItemCollector will be instantiated under.</param>
		public Builder(GameObject parent)
		{
			this.parent = parent;
		}

		public Builder CollectorField(Collider2D collectorField)
		{
			this.collectorField = collectorField;
			return this;
		}


		public Builder ItemFilter(ItemType itemFilter)
		{
			this.itemFilter = itemFilter;
			return this;
		}


		public Builder TargetInventory(Inventory targetInventory)
		{
			this.targetInventory = targetInventory;
			return this;
		}


		public static implicit operator ItemCollector(Builder builder)
		{
			GameObject collectorObject = new GameObject("Item Collector");
			collectorObject.transform.position = builder.parent.transform.position;
			collectorObject.transform.parent = builder.parent.transform;
			Collider2D collectorField = collectorObject.CopyComponent(builder.collectorField);
			collectorField.isTrigger = true;
			ItemCollector itemCollector = collectorObject.AddComponent<ItemCollector>();
			itemCollector.targetInventory = builder.targetInventory;
			itemCollector.collectorField = collectorField;
			itemCollector.itemFilter = builder.itemFilter;

			return itemCollector;
		}
	}
	#endregion
		
}
