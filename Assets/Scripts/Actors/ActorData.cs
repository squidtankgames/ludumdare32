﻿using System.Collections.Generic;

using Spineless;
using UnityEngine;


/// <summary>
/// Represents game objects that have abilities and stats.
/// In scene, all data gets cloned into own instance.
/// </summary>
[System.Serializable]
public class ActorData
{

	#region Fields
	[SerializeField] private int id;
	[SerializeField] private string name;
	[SerializeField] private GameObject prefab;

	[SerializeField] private IntegerRange health;

	[SerializeField] private Inventory inventory;
	[SerializeField] private List<DeathScript> deathActions;
	#endregion


	#region Properties
	public int Id
	{
		get { return this.id; }
	}


	public IntegerRange Health
	{
		get { return this.health; }
	}

		
	public string Name
	{
		get { return this.name; }
	}
	#endregion


	#region Instantiate()
	/// <summary>
	/// Instantiate actor's prefab into the scene.
	/// </summary>
	/// <returns>Instantiated GameObject.</returns>
	public GameObject Instantiate()
	{
		return Instantiate(this.prefab.transform.position, this.prefab.transform.rotation);
	}


	/// <summary>
	/// Instantiate actor's prefab into the scene at the location/rotation of a transform.
	/// </summary>
	/// <param name="transform">Transform that actor will be instantiated at.</param>
	/// <returns>Instantiated GameObject.</returns>
	public GameObject Instantiate(Transform transform)
	{
		return Instantiate(transform.position, transform.rotation);
	}


	/// <summary>
	/// Instantiate actor's prefab into the scene at a specific position and rotation.
	/// </summary>
	/// <param name="position">Vector2 location to instantiate actor to.</param>
	/// <param name="rotation">Rotation of actor once instantiated.</param>
	/// <returns>Instantiated GameObject.</returns>
	public GameObject Instantiate(Vector2 position, Quaternion rotation)
	{
		GameObject instance = this.prefab.Instantiate(position, rotation);

		// Update actor component (add one if none is found).
		ActorComponent actorComponent = instance.GetComponent<ActorComponent>();
		if (actorComponent == null)
			actorComponent = instance.AddComponent<ActorComponent>();
		if (actorComponent != null)
		{
			actorComponent.Data = this;
			actorComponent.Health = (IntegerRange)this.health.Clone();
			actorComponent.Inventory = (Inventory)this.inventory.Clone();
			actorComponent.DeathActions = this.deathActions;
		}
			
		return instance;
	}
	#endregion

}