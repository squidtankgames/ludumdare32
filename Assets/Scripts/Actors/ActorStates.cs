﻿using Spineless;
using UnityEngine;


public partial class ActorComponent
{
		
	#region Fields
	private StateMachine<State.Action> actionState = new StateMachine<State.Action>(State.Action.Idle);
	private StateMachine<State.Jump> jumpState = new StateMachine<State.Jump>(State.Jump.Ground);
	private StateMachine<State.Mobile> mobileState = new StateMachine<State.Mobile>(State.Mobile.Stationary);
	#endregion


	#region Enums
	public static class State
	{
		public enum Action
		{
			Idle,
			Attack,
			Power
		}


		public enum Jump
		{
			Ground,
			Rise,
			Fall
		}


		public enum Mobile
		{
			Stationary,
			Walk,
			Run,
			Dash
		}
	}
	#endregion


	#region Properties
	public StateMachine<State.Action> ActionState
	{
		get { return actionState; }
	}


	public StateMachine<State.Jump> JumpState
	{
		get { return jumpState; }
	}


	public StateMachine<State.Mobile> MobileState
	{
		get { return mobileState; }
	}
	#endregion


	#region Initialization
	private void InitializeStates()
	{
		// Set up all possible ActionState transitions.
		this.actionState.AddTransition(State.Action.Idle, State.Action.Attack, Attacking);
		this.actionState.AddTransition(State.Action.Attack, State.Action.Idle, EndAttack);

		// Set up all possible JumpState transitions.
		this.jumpState.AddTransition(State.Jump.Ground, State.Jump.Rise, Jumping);
		this.jumpState.AddTransition(State.Jump.Ground, State.Jump.Fall, Falling);
		this.jumpState.AddTransition(State.Jump.Rise, State.Jump.Fall, Falling);
		this.jumpState.AddTransition(State.Jump.Fall, State.Jump.Ground, Grounded);

		// Set up all possible MobileState transitions.
		this.mobileState.AddTransition(State.Mobile.Stationary, State.Mobile.Walk, Walking);
		this.mobileState.AddTransition(State.Mobile.Stationary, State.Mobile.Run, Running);
		this.mobileState.AddTransition(State.Mobile.Stationary, State.Mobile.Dash, Dashing);
		this.mobileState.AddTransition(State.Mobile.Walk, State.Mobile.Stationary, Stationary);
		this.mobileState.AddTransition(State.Mobile.Walk, State.Mobile.Dash, Dashing);
		this.mobileState.AddTransition(State.Mobile.Run, State.Mobile.Stationary, Stationary);
		this.mobileState.AddTransition(State.Mobile.Dash, State.Mobile.Stationary, Stationary);
		this.mobileState.AddTransition(State.Mobile.Dash, State.Mobile.Walk, Walking);
		this.mobileState.AddTransition(State.Mobile.Dash, State.Mobile.Run, Running);
	}
	#endregion


	#region Action States
	private void Idle()
	{
		switch (mobileState.CurrentState)
		{
			case State.Mobile.Stationary:
				Stationary();
				break;
			case State.Mobile.Walk:
				Walking();
				break;
			case State.Mobile.Run:
				Running();
				break;
			case State.Mobile.Dash:
				Dashing();
				break;
		}
	}


	private void Attacking()
	{
		switch (mobileState.CurrentState)
		{
			case State.Mobile.Stationary:
				StartAttack();
				break;
			case State.Mobile.Walk:
				Walking();
				break;
		}
	}


	private void ActivatingPower()
	{
		// TODO: All cases of power animation based on jump state.
	}
	#endregion


	#region Jump States
	private void Jumping()
	{
		if (this.actionState.CurrentState == State.Action.Attack)
			return;

		this.Animator.Play("Jump");
	}


	private void Falling()
	{
		if (this.actionState.CurrentState == State.Action.Attack)
			return;
		
		this.Animator.Play("Fall");
	}


	private void Grounded()
	{
		if (this.actionState.CurrentState == State.Action.Attack)
			return;

		switch (this.mobileState.CurrentState)
		{
			case State.Mobile.Stationary:
				this.Animator.Play("Idle");
				break;
			case State.Mobile.Walk:
				this.Animator.Play("Walk");
				break;
			case State.Mobile.Run:
				this.Animator.Play("Run");
				break;
		}
	}
	#endregion


	#region Mobile States
	private void Stationary()
	{
		if (this.actionState.CurrentState == State.Action.Attack)
			return;

		switch (this.jumpState.CurrentState)
		{
			case State.Jump.Ground:
				this.Animator.Play("Idle");
				break;
		}
	}


	private void Walking()
	{
		switch (this.jumpState.CurrentState)
		{
			case State.Jump.Ground:
				this.Animator.Play("Walk");
				break;
		}
	}


	private void Running()
	{
		switch (this.jumpState.CurrentState)
		{
			case State.Jump.Ground:
				this.Animator.Play("Run");
				break;
		}
	}


	private void Dashing()
	{
		switch (this.jumpState.CurrentState)
		{
			case State.Jump.Ground:
				this.Animator.Play("Dash");
				break;
		}
	}
	#endregion


	#region Attack States
	private void StartAttack()
	{
		if (this.jumpState.CurrentState == State.Jump.Ground
			&& this.mobileState.CurrentState == State.Mobile.Stationary)
			this.Animator.Play("StartAttack");
	}


	private void EndAttack()
	{
		if (this.jumpState.CurrentState == State.Jump.Ground
			&& this.mobileState.CurrentState == State.Mobile.Stationary)
			this.Animator.Play("EndAttack");
	}
	#endregion

}